README - TODO

# Installing Lity through Composer

Next tell composer which libraries to install. You have two options:
A: Use the [Composer Merge plugin](https://github.com/wikimedia/composer-merge-plugin)
to include the Lity modules's composer.libraries.json (recommended)

This is the recommended way which is more convenient and also ensures that the
required libraries will always be updated when the module is updated.

1. The merging is accomplished by the aid of the
  [Composer Merge plugin](https://github.com/wikimedia/composer-merge-plugin)
  available on GitHub, so from the project directory run:
   
  `composer require wikimedia/composer-merge-plugin`

2. Edit the "composer.json" file of your website and under the **"extra": {**
   section add:
   ```json
   "merge-plugin": {
     "include": [
	   "web/modules/contrib/lity/composer.libraries.json"
	 ]
   }
   ```
   Note: the `web` represents the folder where drupal lives like: ex. `docroot`.

   From now on, every time the "composer.json" file is updated, it will also 
   read the content of "composer.libraries.json" file located at
   `web/modules/contrib/lity/` and update accordingly.

B: Add packages manually

Under the "repositories" section of composer.json, add the following:
```json
{
  "repositories": {
	"jsor/lity": {
	  "type": "package",
	  "package": {
		"name": "jsor/lity",
		"version": "2.3.1",
		"type": "drupal-library",
		"dist": {
		  "url": "https://github.com/jsor/lity/archive/v2.3.1.zip",
		  "type": "zip"
		}
	  }
	}
  },
  "require": {
	"jsor/lity": "2.3.1"
  }
}
```

2. Run `composer require jsor/lity` 

Run `composer require drupal/lity` to install this module.
