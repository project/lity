<?php

namespace Drupal\lity\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Defines a base block implementation that most blocks plugins will extend.
 *
 * This abstract class provides the generic block configuration form, default
 * block settings, and handling for general user-defined block visibility
 * settings.
 */
abstract class LityBlockBase extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    return $build;
  }

}
