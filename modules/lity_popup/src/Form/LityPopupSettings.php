<?php

namespace Drupal\lity_popup\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class.
 *
 * Description.
 *
 * @package
 */
class LityPopupSettings extends ConfigFormBase {
  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'lity_popup.settings';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'lity_popup_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['popup_body'] = [
      '#title' => $this->t('Popup body content'),
      '#type' => 'textfield',
      '#default_value' => $config->get('lity.popup_body'),
      '#required' => TRUE,
    ];

    $form['popup_yes'] = [
      '#title' => $this->t('Button text for yes'),
      '#type' => 'textfield',
      '#default_value' => $config->get('lity.popup_yes'),
      '#required' => TRUE,
    ];

    $form['popup_no'] = [
      '#title' => $this->t('Button text for no'),
      '#type' => 'textfield',
      '#default_value' => $config->get('lity.popup_no'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->config(static::SETTINGS)
      ->set('lity.popup_body', $form_state->getValue('popup_body'))
      ->set('lity.popup_yes', $form_state->getValue('popup_yes'))
      ->set('lity.popup_no', $form_state->getValue('popup_no'))
      ->save();
  }

}
