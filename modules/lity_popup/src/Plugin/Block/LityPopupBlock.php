<?php

namespace Drupal\lity_popup\Plugin\Block;

use Drupal\lity\Plugin\Block\LityBlockBase;

/**
 * Provides a 'Lity Popup Block' block.
 *
 * @Block(
 *  id = "lity_popup_block",
 *  admin_label = @Translation("Lity Popup Block"),
 * )
 */
class LityPopupBlock extends LityBlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config_form_values = \Drupal::service('config.factory')
      ->get('lity_popup.settings');

    $build = [
      '#theme' => 'lity_popup_block',
      '#lity_popup_body' => $config_form_values->get('lity.popup_body'),
      '#lity_popup_yes' => $config_form_values->get('lity.popup_yes'),
      '#lity_popup_no' => $config_form_values->get('lity.popup_no'),
      '#attached' => ['library' => ['lity_popup/lity']],
    ];

    return $build;
  }

}
