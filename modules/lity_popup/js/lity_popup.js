(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.lityPopupDisplay = {
    attach: function (context, settings) {

      // Only load the popup once on the page.
      $(document, context).once('lityPopupDisplay').each(function () {

        function setLityPopupCookie($cookieName) {
          $.cookie($cookieName, 'TRUE', { path: '/', expires: 30 });
        }

        function getLityPopupCookie($cookieName) {
          return $.cookie($cookieName);
        }

        function renderLity() {
          var lightbox = lity('#lity-inline');

          $('.lity-close').click(function () {
            lightbox.close();
          });
        }

        function displayLity() {
          setTimeout(renderLity, 2000);
        }

        if (!getLityPopupCookie(   'lity_popup_displayed')) {
          setLityPopupCookie('lity_popup_displayed');
          displayLity();
        }
      });

    }

  };

})(jQuery, window.Drupal);


